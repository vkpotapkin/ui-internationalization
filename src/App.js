import React from "react";
import "./App.css";
import { useTranslation } from "react-i18next";
import axios from 'axios'
import {InternationalContent} from "./internationalContent";

function App() {
    const { t, i18n } = useTranslation();

    let axiosAjax = axios.create()
    let axiosResponse = "";

    function saveLanguage(language) {
        let url = "/savePreferredLocale"
        let dataParam = {language}
        axiosAjax.post(url, dataParam)
            .then((response) => {
            axiosResponse = response.data;
        });
    }

    function getLanguageFromServer() {
        let url = "/getPreferredLocale"
        axiosAjax.post(url).then((response) => {
            axiosResponse = response.data;
            changeLanguage(axiosResponse)
            });
    }

    function sendToKafkaInternationalContent () {
        let url = "/sendToKafkaInternationalContent";
        let dataParam = {
            text: 'textMessage',
            date: '2021-09-04T10:42:51.377',
            amount: '1147.55'
        };

        axios.post(url, dataParam).then((response) => {
            axiosResponse = response.data;
        })
    };

    const changeLanguage = (language) => {
        i18n.changeLanguage(language);
    };

    const changeLanguageWithServerSave = (language) => {
        saveLanguage(language)
        i18n.changeLanguage(language);
    };

    return (
        <div className="App">
            <br/>
            <button onClick={() => changeLanguageWithServerSave("en")}>en</button>
            <button onClick={() => changeLanguageWithServerSave("ru")}>ru</button>
            <button onClick={() => changeLanguageWithServerSave("ru-RU")}>ru-RU</button>
            <button onClick={() => changeLanguageWithServerSave("de")}>de</button>
            <button onClick={() => changeLanguageWithServerSave("zh")}>zh</button>
            <button onClick={() => changeLanguageWithServerSave("zh-Hans-CN")}>zh-Hans-CN</button>
            <hr />
            <div><h1>{t("title")}</h1></div>
            <div>{t("description.part1")}</div>
            <div>{t("description.part2")}</div>
            <br/>
            <div>{t("currentDate", { datePlaceHolder: new Date()})}</div>
            <div>{t("value", { numberPlaceHolder: new Number(1147.55) })}</div>
            <br/>
            <div>language: {i18n.language}</div>
            <br/>
            <button onClick={() => getLanguageFromServer()}>получить локаль с сервера</button>
            <br/>
            <hr />
            <div><h1>{t("titleBackEnd")}</h1></div>
            <br/>
            <InternationalContent />
            <br/>
            <hr />
            <br/>
            <button onClick={() => sendToKafkaInternationalContent()}>Отправить сообщение в kafka</button>
            <br/>
            <br/>
            <hr />
        </div>
    );
}

export default App;