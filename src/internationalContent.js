import React, { Component } from 'react'
import axios from "axios";

export class InternationalContent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {textValue: ''}
    }

    getInternationalContent = (url) => {
        let dataParam = {
            text:'textMessage',
            date: '2021-09-04T10:42:51.377',
            amount : '1147.55'
        };

        axios.post(url, dataParam).then((response) => {
            this.setState({
                textValue: JSON.stringify(response.data, null, 2)
            })
        })
    };

    render () {
        return (
            <div>
                <button onClick={() => this.getInternationalContent("/getInternationalContent")}>Получить данные с сервера</button>
                <br/>
                <br/>
                <button onClick={() => this.getInternationalContent("/httpClientRequest")}>Отправить запрос через httpclient</button>
                <br/>
                <br/>
                <textarea rows={20} cols={70} {...this.props} placeholder={this.state.textValue}/>
            </div>
        )
    }
}