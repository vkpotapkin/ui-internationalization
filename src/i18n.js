import i18n from 'i18next'
import Backend from 'i18next-http-backend'
import Detection from 'i18next-browser-languagedetector'
import { initReactI18next } from 'react-i18next'

// импорты для локального варинта src/locales
import de from './locales/de.json';
import en from './locales/en.json';
import ru from './locales/ru.json';
import ruRU from './locales/ru-RU.json';
import zh from './locales/zh.json';
import zhHansCN from './locales/zh-Hans-CN.json';

i18n
    // Enables the i18next backend
    //Для варианта с запросами к http серверу где файлы лежат в public/locales
    .use(Backend)

    // Enable automatic language detection
    .use(Detection)

    // Enables the hook initialization module
    .use (initReactI18next)
    .init({

        //Настройки для варианта Backend с запросами к http серверу где файлы лежат в public/locales
        backend:{ loadPath: '/locales/{{lng}}.json' },

        //Вариант с компиляцией где файлы лежат в src/locales. Переопределяет Backend вариант. В случае использования Backend варианта закоментировать!!!
        resources:{
            de :{
                'translation' : de
            },
            en :{
                'translation' : en
            },
            ru :{
                'translation' : ru
            },
            'ru-RU' :{
                'translation' : ruRU
            },
            zh :{
                'translation' : zh
            },
            'zh-Hans-CN' :{
                'translation' : zhHansCN
            }
        },
        detection: {
            order: ['cookie','navigator'],
            lookupCookie: 'preferredlocale'
        },
        fallbackLng: 'ru',
        debug: true,
        interpolation: {
            format: function(value, format, lng) {
                if(value instanceof Date) {
                    if(format=='short'){
                        return new Intl.DateTimeFormat(lng, { localeMatcher: 'lookup', dateStyle: 'short', timeStyle: 'short' }).format(value)
                    }
                    if(format=='medium'){
                        return new Intl.DateTimeFormat(lng, { localeMatcher: 'lookup', dateStyle: 'medium', timeStyle: 'medium' }).format(value)
                    }
                    if(format=='long'){
                        return new Intl.DateTimeFormat(lng, { localeMatcher: 'lookup', dateStyle: 'long', timeStyle: 'long' }).format(value)
                    }
                    if(format=='full'){
                        return new Intl.DateTimeFormat(lng, { localeMatcher: 'lookup', dateStyle: 'full', timeStyle: 'full' }).format(value)
                    }
                    else{
                        return new Intl.DateTimeFormat(lng, { localeMatcher: 'lookup' }).format(value)
                    }
                }
                if(value instanceof Number) {
                    return new Intl.NumberFormat(lng).format(value)
                }
            }
        }
    })
export default i18n;


